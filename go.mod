module github.com/tidepool-org/tide-whisperer

go 1.12

require (
	github.com/daaku/go.httpgzip v0.0.0-20180202095102-86d27ccd810b
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/google/uuid v1.1.1
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/mux v1.7.3 // indirect
	github.com/gorilla/pat v1.0.1
	github.com/tidepool-org/go-common v0.5.0
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
